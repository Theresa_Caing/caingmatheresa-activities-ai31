<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\playlist;
class PlaylistController extends Controller
{
    public function displayPlaylists(){
        return DB::table('playlists')->get();
    }


    public function store(Request $request){

        $newPlaylist = new playlist();
        $newPlaylist->name = $request->name;
        $newPlaylist>save();
        return $newPlaylist;
}
}
